################################################################################
## This Makefile has been made carefully and artistically by Mehrad           ##
## Mahmoudian.                                                                ##
## If you don't know what each part does, and the comments are not enough     ##
## please consider asking :)                                                  ##
################################################################################

# To debug the issues of this Makefile, start with checking if you have
# mistakenly used spaces instead of tabs (It shows the presence of tabs with
# `^I` and line endings with `$`):
#  cat -e -t -v Makefile
# Source: https://stackoverflow.com/a/16945143/1613005


SHELL = /bin/sh

# define a variable to store the dependencies
REQUIRED_BINS := sh cargo

# define the path highlighter should be installed in
PREFIX ?= /usr/bin

# define a newline character to be used in messages
define LINEBREAK


endef

DEBUG ?=

ifdef DEBUG
    RELEASE :=
	TARGET :=debug
else
	RELEASE :=--release
	TARGET :=release
endif


COLOR ?= TRUE

ifeq ($(COLOR),TRUE)
	COLOR_RESET :=$(shell tput sgr0)
	COLOR_ERROR :=$(shell tput setaf 1)
	COLOR_SUCCESS :=$(shell tput setaf 2)
	COLOR_MESSAGE :=$(shell tput setaf 3)
else
	COLOR_RESET :=
	COLOR_ERROR :=
	COLOR_SUCCESS :=
	COLOR_MESSAGE :=
endif


.PHONY: install


help:
	$(info --------------------------------------------------------------------------------)
	$(info Available arguments:)
	$(info - "make check"   to check if you have all dependencies installed)
	$(info - "make build"   to compile the program. To have debug version, use $(COLOR_MESSAGE)"make build DEBUG=T"$(COLOR_RESET))
	$(info - "make install" to install in the defined path: '$(PREFIX)')
	$(info - "make clean"   to clean the build files in this folder (will not affect other folders))
	$(info - "make remove"  to uninstall (remove))
	$(info - "make help"    to show this help)
	$(info )
	$(info You can turn off colorizing the make output by $(COLOR_MESSAGE)"COLOR=FALSE"$(COLOR_RESET))
	$(info --------------------------------------------------------------------------------)
#	to suppress the "make: 'all' is up to date." message
	@:


check:
#	checking if the dependencies are me# checking if the dependencies are mett
	$(foreach bin,$(REQUIRED_BINS),\
		$(if $(shell command -v $(bin) 2> /dev/null),$(info $(COLOR_SUCCESS)[Ok]$(COLOR_RESET) Found `$(bin)`),$(error ${LINEBREAK}$(COLOR_ERROR)[Error]$(COLOR_RESET) Missing Dependency. Please install `$(bin)`)))
	@if [ -f "${PREFIX}/highlighter" ]; then \
		echo "$(COLOR_MESSAGE)[NOTE]$(COLOR_RESET) highlighter is already installed"; \
	else \
		echo "$(COLOR_MESSAGE)[NOTE]$(COLOR_RESET) highlighter is NOT installed yet"; \
	fi
	$(info ) 
	@:


clean: check
	$(info )
	cargo clean
	@:


build: check
	$(info )
	cargo build $(RELEASE)
	@:


install:
#   make sure the binary is already compiled
	@if [ ! -f "target/${TARGET}/highlighter" ] ; then \
		echo -e "${COLOR_ERROR}[Error]${COLOR_RESET} You should first compile (a.k.a build) the software. Try:\n make build \n"; \
		exit 1; \
	fi
# installing the binary in correct place with correct permissions
	install --target "${PREFIX}" -D -m755 target/${TARGET}/highlighter
#   check if it is copied to the destination
	@if [ -f "${PREFIX}/highlighter" ] ; then \
		echo -e "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Successfully installed. Now you can use highlighter as a command"; \
	else \
		echo -e "${COLOR_ERROR}[Error]${COLOR_RESET} Pathetically failed to install :("; \
	fi
	@:


remove uninstall:
	@if [ -f "${PREFIX}/highlighter" ]; then \
		echo -e "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Found the highlighter. Going to remove ..."; \
		rm "${PREFIX}/highlighter"; \
		if [ -f "${PREFIX}/highlighter" ]; then \
			echo -e "${COLOR_ERROR}[Error]${COLOR_RESET} Pathetically failed to remove"; \
		else \
			echo -e "${COLOR_SUCCESS}[Ok]${COLOR_RESET} Successfully removed"; \
		fi \
	else \
		echo -e "${COLOR_ERROR}[Error]${COLOR_RESET} highlighter was not found to be removed/uninstalled!"; \
	fi
	@:



all: check clean build install
