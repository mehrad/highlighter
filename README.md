# Highlighter


This is a light-weight software written in Rust to read some input text from standard input (stdin), apply some color highlighting based on the provided pattern. You can either provide the word/phrase you want to highlight or you can provide a regular expression. Both are shown in this example:


**Quick links:**

- [How to use](#how-to-use)
    - [Example](#example)
- [Installation](#installation)
    - [Linux](#linux)
    - [macOS](#macos)
    - [Build from source](#build-from-source)
- [Current status](#current-status)


## How to use

The usage is very straightforward, just pipe the text into `hightlighter` and define the colors as arguments in short form or long form along with a pattern like the example at the begining of this page.

The available colors are:

| color name | short form | long form |
|:---------- |:---------- |:--------- |
| Red        | -r         | --red     |
| Green      | -g         | --green   |
| Blue       | -b         | --blue    |
| Cyan       | -c         | --cyan    |
| Magenta    | -m         | --magenta |
| yellow     | -y         | --yellow  |
| black      | -k         | --black   |

**Note** that the colors displayed in your terminal depends on the color profile you have and they might not necessaily match the colors you see here.


### Example

```sh
locale | highlighter  --blue="=" --red="_U.*" --yellow "NAME" --green="_ME.*"
```

 <img src="https://codeberg.org/mehrad/highlighter/raw/branch/main/doc/highlighter-before-after.png" alt="Illustrate how highlighter works" width="500" height="526"> 

## Installation

### Linux

#### Package manager

**NOTICE:** It will be available on Arch User Repository (AUR) immediately after certain papercut issued are ironed out.
For the time being you can use [build it from source](https://codeberg.org/mehrad/highlighter#build-from-source)

<!--
I'm just releasing this software and code and as the result there is no distro that have adopted the software.
But for certain distros I have made it available in their user repository:

```sh
yay -S highlighter
```
-->

### macOS

**NOTICE:** This software is stull not in brew, but it is in my plans to put it there as well.
For the time being you can use [build it from source](https://codeberg.org/mehrad/highlighter#build-from-source)

<!--
```sh
brew install highlighter
```
-->

### Build from source 

#### Dependencies

You need to have Rust installed. My suggestion is to use the rustup which is suggested by the [official Rust language](https://www.rust-lang.org/learn/get-started):

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup update
```

#### Building

Just use these three lines to download the source code and build it:

```sh
git clone https://codeberg.org/mehrad/highlighter.git
cd highlighter
make help    # to get what you can do with the make
make build   # to compile it
make install # to install the compiled binary
```

## Current status

Still in development, but the basic ideas are in place and basic functionality works just fine. Enjoy! :)

## Contribution

If you are willing to help and contribute to the project, please consider the following:

- I'm one of those people who think no amount of commenting is excessive commenting, so please comment the code at least to some extent
- Please try to keep the naming style that is already in the code (style of variable names, commenting, ...)
- Please use the [EditorConfig](https://editorconfig.org) so that we all have some basic uniform formatting style.
- bare in mind that I'm not [yet] a seasoned Rust developer, so instead of agruing, try teaching me or pointing me to the correct direction

